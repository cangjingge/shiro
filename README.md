# shiro

#### 介绍
shiro快速使用

只需要自己实现实现bootstarp引导包下面的三个类；

1.AbstractShiroService（该抽象类中有两个方法需要实现，一个根据登录名查询用户基本数据，一个用于初始化所有url访问时需要的角色列表）

还有一个刷新权限缓存的方法updatePermission，在修改了系统权限配置后可以调用刷新权限缓存数据。

2.ShiroUrlRoles（url和角色关联数据，为一对多）

3.ShiroUser（用户数据，其中需要保护该用户有哪些权限）

三者实现在demo包下都有。自己根据业务实现即可