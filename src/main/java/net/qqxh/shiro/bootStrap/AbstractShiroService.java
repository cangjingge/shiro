package net.qqxh.shiro.bootStrap;

import org.apache.shiro.spring.web.ShiroFilterFactoryBean;
import org.apache.shiro.web.filter.mgt.DefaultFilterChainManager;
import org.apache.shiro.web.filter.mgt.PathMatchingFilterChainResolver;
import org.apache.shiro.web.servlet.AbstractShiroFilter;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created by jason on 2019/2/2 10:23
 */
public abstract class AbstractShiroService {


    ShiroFilterFactoryBean shiroFilterFactoryBean;

    /**
     * 加载用户信息并封装该用户的角色信息
     *
     * @param loginName
     * @return
     */
    public abstract ShiroUser getUserByLoginName(String loginName);

    /**
     * 获取系统所有url权限角色信息
     *
     * @return
     */
    public abstract List<ShiroUrlRoles> getAllUrlRolesList();

    /**
     * 更新系统权限缓存数据方法。后台配置改变之后需要调用该方法进行刷新缓存信息
     */
    public void updatePermission() {

        synchronized (shiroFilterFactoryBean) {

            AbstractShiroFilter shiroFilter = null;
            try {
                shiroFilter = (AbstractShiroFilter) shiroFilterFactoryBean
                        .getObject();
            } catch (Exception e) {
                throw new RuntimeException(
                        "get ShiroFilter from shiroFilterFactoryBean error!");
            }

            PathMatchingFilterChainResolver filterChainResolver = (PathMatchingFilterChainResolver) shiroFilter
                    .getFilterChainResolver();
            DefaultFilterChainManager manager = (DefaultFilterChainManager) filterChainResolver
                    .getFilterChainManager();

            // 清空老的权限控制
            manager.getFilterChains().clear();
            shiroFilterFactoryBean.getFilterChainDefinitionMap().clear();
            shiroFilterFactoryBean.setFilterChainDefinitionMap(getFilterChainDefinitionMap());
            // 重新构建生成
            Map<String, String> chains = shiroFilterFactoryBean.getFilterChainDefinitionMap();
            for (Map.Entry<String, String> entry : chains.entrySet()) {
                String url = entry.getKey();
                String chainDefinition = entry.getValue().trim()
                        .replace(" ", "");
                manager.createChain(url, chainDefinition);
            }

            System.out.println("更新权限成功！！");
        }
    }

    public void setShiroFilterFactoryBean(ShiroFilterFactoryBean shiroFilterFactoryBean) {
        this.shiroFilterFactoryBean = shiroFilterFactoryBean;
    }

    /**
     * 获取系统所有权限信息
     *
     * @return
     */
    public Map<String, String> getFilterChainDefinitionMap() {
        List<ShiroUrlRoles> list = getAllUrlRolesList();
        Map<String, String> filterChainDefinitionMap = new HashMap();
        if (list != null) {
            for (ShiroUrlRoles shiroUrlRoles : list) {
                Set<String> roles = shiroUrlRoles.getRoles();
                if (roles != null) {
                    filterChainDefinitionMap.put(shiroUrlRoles.getUrl(), "roles" + roles.toString());
              /*      if (roles.size() == 1) {

                    } else if (roles.size() > 1) {
                        String index = "[\"";
                        int flag = 0;
                        for (String s : roles) {
                            index += s;
                            flag++;
                            if(flag!=roles.size()){
                                index +=  ",";
                            }
                        }
                        index += "\"]";
                        filterChainDefinitionMap.put(shiroUrlRoles.getUrl(), "roles" + index);
                    }*/

                }
            }
        }
        return filterChainDefinitionMap;
    }
}
