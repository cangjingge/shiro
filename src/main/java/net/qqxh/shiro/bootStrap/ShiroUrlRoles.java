package net.qqxh.shiro.bootStrap;

import java.util.Set;

/**
 * url和权限关联bean接口
 * Created by jason on 2019/2/2 11:09
 */
public interface ShiroUrlRoles {
    String getUrl();
    /**
     * url所需要的角色列表
     * @return
     */
    Set<String> getRoles();
}
