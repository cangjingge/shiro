package net.qqxh.shiro.bootStrap;

import java.util.Set;

/**
 * 用户信息接口
 * Created by jason on 2019/2/2 10:22
 */
public interface ShiroUser {
    String getLoginName();
    String getPwd();
    Set<String>getRoles();
}
