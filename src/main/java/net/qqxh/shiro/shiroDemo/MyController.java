package net.qqxh.shiro.shiroDemo;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Created by jason on 2019/2/2 11:18
 */
@Controller
@RequestMapping("/test")
public class MyController {
    @ResponseBody
    @RequestMapping("/test")
    public Object test() {
        return "success";
    }

    @ResponseBody
    @RequestMapping("/test2")
    public Object test2() {
        return "success";
    }
}
