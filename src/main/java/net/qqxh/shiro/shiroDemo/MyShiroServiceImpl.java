package net.qqxh.shiro.shiroDemo;

import net.qqxh.shiro.bootStrap.AbstractShiroService;
import net.qqxh.shiro.bootStrap.ShiroUrlRoles;
import net.qqxh.shiro.bootStrap.ShiroUser;

import org.springframework.stereotype.Service;

import java.util.*;

/**
 * Created by jason on 2019/2/2 10:56
 */
@Service("shiroService")
public class MyShiroServiceImpl extends AbstractShiroService {
    @Override
    public ShiroUser getUserByLoginName(String loginName) {
        /*查询到该用户*/
        MyShiroUser myUser = new MyShiroUser();
        myUser.setLoginName("jason");
        myUser.setPwd("1");
        /*查询到该用户的角色*/
        Set<String> roles = new HashSet<>();
        roles.add("testrole3");

        myUser.setRoles(roles);
        return myUser;
    }

    /**
     * 拿到系统中所有url对应的角色信息
     *
     * @return
     */
    @Override
    public List<ShiroUrlRoles> getAllUrlRolesList() {
        List<ShiroUrlRoles> list = new ArrayList();

        MyShiroUrlRoles myShiroUrlRoles = new MyShiroUrlRoles();

        myShiroUrlRoles.setUrl("/test/test");
        Set<String> set = new HashSet();
        set.add("testrole");
        set.add("testrole2");
        myShiroUrlRoles.setRoles(set);


        MyShiroUrlRoles myShiroUrlRoles2 = new MyShiroUrlRoles();
        myShiroUrlRoles2.setUrl("/test/test2");
        Set<String> set2 = new HashSet();
        set2.add("testrole2");
        set2.add("testrole3");
        set2.add("testrole");
        myShiroUrlRoles2.setRoles(set2);

     /*   MyShiroUrlRoles myShiroUrlRoles3 = new MyShiroUrlRoles();
        myShiroUrlRoles3.setUrl("/updatePermission");
        Set<String> set3 = new HashSet();
        set3.add("admin");
        myShiroUrlRoles3.setRoles(set3);*/


        list.add(myShiroUrlRoles2);
        list.add(myShiroUrlRoles);
        return list;
    }
}
