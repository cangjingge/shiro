package net.qqxh.shiro.shiroDemo;

import net.qqxh.shiro.bootStrap.ShiroUrlRoles;

import java.util.Set;

/**
 * url和权限关联bean
 * Created by 18060774 on 2019/2/2 15:16
 */
public class MyShiroUrlRoles implements ShiroUrlRoles {

    private String url;
    private Set roles;

    public void setUrl(String url) {
        this.url = url;
    }

    public void setRoles(Set roles) {
        this.roles = roles;
    }

    @Override
    public String getUrl() {
        return this.url;
    }

    @Override
    public Set getRoles() {
        return this.roles;
    }
}
