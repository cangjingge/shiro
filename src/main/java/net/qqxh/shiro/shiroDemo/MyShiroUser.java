package net.qqxh.shiro.shiroDemo;

import net.qqxh.shiro.bootStrap.ShiroUser;

import java.io.Serializable;
import java.util.Set;

/**
 * Created by jason on 2019/2/2 11:15
 */
public class MyShiroUser implements ShiroUser, Serializable {
    private static final long serialVersionUID = -1373760761780840081L;

    private String pwd;

    private Set<String> roles;

    private String loginName;


    public void setPwd(String pwd) {
        this.pwd = pwd;
    }

    public void setRoles(Set<String> roles) {
        this.roles = roles;
    }

    @Override
    public String getLoginName() {
        return this.loginName;
    }

    public void setLoginName(String loginName) {
        this.loginName = loginName;
    }

    @Override
    public String getPwd() {
        return this.pwd;
    }

    @Override
    public Set<String> getRoles() {
        return this.roles;
    }
}
